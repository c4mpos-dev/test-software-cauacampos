from src.calculator import Calculator
from src.operations.sum import SumOperation
from src.operations.sub import SubOperation

calculator = Calculator(SumOperation(), SubOperation())

operaration_1 = calculator.addition(2, 5, True)
operaration_2 = calculator.subtraction(5, 3, True)

print(f"Operation 1: {operaration_1}")
print(f"Operation 2: {operaration_2}")